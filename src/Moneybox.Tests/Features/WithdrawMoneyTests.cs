﻿namespace Moneybox.Tests.Features
{
    using System;
    using App;
    using App.DataAccess;
    using App.Features;
    using AutoFixture;
    using Moq.AutoMock;
    using TestStack.BDDfy;
    using Xunit;

    public class WithdrawMoneyTests : TestBase
    {

        private readonly WithdrawMoney _withdrawMoney;
       
        public WithdrawMoneyTests()
        {
            _withdrawMoney = AutoMocker.CreateInstance<WithdrawMoney>();
        }

        [Fact]
        public void MoneyIsWithdrawn()
        {
            this.Given(_ => GivenExpectedToAccountSetUp())
                .And(_ => GivenOverrideFromBalanceTo(300))
                .And(_ => GivenGetAccountByIdToSetup())
                .And(_ => GivenAmountSetTo(200))
                .When(_ => WhenWeCallExecute())
                .BDDfy();
        }

        [Fact]
        public void InsufficientFunds()
        {
            this.Given(_ => GivenExpectedToAccountSetUp())
                .And(_ => GivenOverrideFromBalanceTo(0))
                .And(_ => GivenGetAccountByIdToSetup())
                .And(_ => GivenAmountSetTo(200))
                .When(_ => WhenCallExecutThenExpectInsufficientAmount())
                .BDDfy();
        }

        private void WhenWeCallExecute()
        {
            _withdrawMoney.Execute(ExpectedTo.Id, Amount);
        }

        private void WhenCallExecutThenExpectInsufficientAmount()
        {
            Assert.Throws<InvalidOperationException>(() => _withdrawMoney.Execute(ExpectedTo.Id, Amount));
        }
    }
}
