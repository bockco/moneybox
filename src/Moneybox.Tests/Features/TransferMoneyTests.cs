﻿namespace Moneybox.Tests.Features
{
    using System;
    using App;
    using App.DataAccess;
    using App.Domain.Services;
    using App.Features;
    using AutoFixture;
    using Moq;
    using Moq.AutoMock;
    using TestStack.BDDfy;
    using Xunit;

    public class TransferMoneyTests : TestBase
    {
        private readonly TransferMoney _transferMoney;   

        public TransferMoneyTests()
        {
            AutoMocker.GetMock<INotificationService>().Setup(x => x.NotifyFundsLow(It.IsAny<string>())).Verifiable();
            AutoMocker.GetMock<INotificationService>().Setup(x => x.NotifyApproachingPayInLimit(It.IsAny<string>())).Verifiable();
            _transferMoney = AutoMocker.CreateInstance<TransferMoney>();
        }

        [Fact]
        public void TransferMoneyUpdatedFromTo()
        {
            this.Given(_ => GivenSuccessTransactionSetup())
                .When(_ => WhenWeCallExecute())
                .Then(_ => ThenFromCallShouldExpectParameters(ExpectedFrom))
                .BDDfy();
        }

        [Fact]
        public void InsufficientLimit()
        {
            this.Given(_ => GivenSuccessTransactionSetup())
                .And(_ => GivenAmountSetTo(4001))
                .When(_ => WhenCallExecutThenExpectInsufficientAmount())
                .BDDfy();
        }

        [Fact]
        public void OverPayinLimit()
        {
            this.Given(_ => GivenSuccessTransactionSetup())
                .And(_ => GivenOverrideToPaidinTo(5000))
                .And(_ => GivenAmountSetTo(0))
                .When(_ => WhenCallExecuteThenExpectOverPayinLimit())
                .BDDfy();
        }

        [Fact]
        public void VerifyNotifyFundsLow()
        { 
            this.Given(_ => GivenSuccessTransactionSetup())
                .And(_ => GivenOverrideFromBalanceFrom(300))
                .And(_ => GivenAmountSetTo(200))
                .When(_ => WhenWeCallExecute())
                .Then(_ => ThenNotifyLowFundsCalled())
                .BDDfy();
        }

        [Fact]
        public void VerifyNotifyApproachingPayInLimit()
        {
            this.Given(_ => GivenSuccessTransactionSetup())
                .And(_ => GivenOverrideToPaidinTo(4000))
                .And(_ => GivenAmountSetTo(0))
                .When(_ => WhenWeCallExecute())
                .Then(_ => ThenNotifyApproachingPayInLimitCalled())
                .BDDfy();
        }

        private void WhenWeCallExecute()
        {
            _transferMoney.Execute(ExpectedFrom.Id, ExpectedTo.Id, Amount);
        }

        private void WhenCallExecutThenExpectInsufficientAmount()
        {

            Assert.Throws<InvalidOperationException>(() => _transferMoney.Execute(ExpectedFrom.Id, ExpectedTo.Id, Amount));
        }

        private void WhenCallExecuteThenExpectOverPayinLimit()
        {
            Assert.Throws<InvalidOperationException>(() => _transferMoney.Execute(ExpectedFrom.Id, ExpectedTo.Id, Amount));
        }

        private void ThenFromCallShouldExpectParameters(Account expectedFrom)
        {
            AutoMocker.Verify<IAccountRepository>(x => x.Update(expectedFrom), Times.Once);
        }

        private void ThenNotifyApproachingPayInLimitCalled()
        {
            AutoMocker.GetMock<INotificationService>().Verify(x => x.NotifyApproachingPayInLimit(It.IsAny<string>()), Times.Once);
        }

        private void ThenNotifyLowFundsCalled()
        {
            AutoMocker.GetMock<INotificationService>().Verify(x => x.NotifyFundsLow(It.IsAny<string>()), Times.Once);
        }
    }
}
