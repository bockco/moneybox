﻿namespace Moneybox.Tests.Features
{
    using App;
    using App.DataAccess;
    using AutoFixture;
    using Moq.AutoMock;

    public class TestBase
    {
        public AutoMocker AutoMocker;
        public Fixture Fixture;
        public decimal Amount;
        public Account ExpectedTo;
        public Account ExpectedFrom;

        public TestBase()
        {
            AutoMocker = new AutoMocker();
            Fixture = new Fixture();
            Amount = 0;
        }

        public void GivenOverrideToPaidinTo(int newToPaidinAmount)
        {
            ExpectedTo.PaidIn = newToPaidinAmount;
        }

        public void GivenSuccessTransactionSetup()
        {
            GivenExpectedFromAccountSetUp();
            GivenGetAccountByIdFromSetup();

            GivenExpectedToAccountSetUp();
            GivenGetAccountByIdToSetup();
        }

        public void GivenOverrideFromBalanceTo(decimal newBalance)
        {
            ExpectedTo.Balance = newBalance;
        }

        public void GivenOverrideFromBalanceFrom(decimal newBalance)
        {
            ExpectedFrom.Balance = newBalance;
        }

        public void GivenExpectedToAccountSetUp()
        {
            ExpectedTo = Fixture.Build<Account>().Create();
        }

        public void GivenGetAccountByIdFromSetup()
        {
            AutoMocker.GetMock<IAccountRepository>().Setup(x => x.GetAccountById(ExpectedFrom.Id)).Returns(ExpectedFrom);
        }

        public void GivenExpectedFromAccountSetUp()
        {
            ExpectedFrom = Fixture.Build<Account>().Create();
        }

        public void GivenAmountSetTo(int newPayinAmount)
        {
            Amount = newPayinAmount;
        }

        public void GivenGetAccountByIdToSetup()
        {
            AutoMocker.GetMock<IAccountRepository>().Setup(x => x.GetAccountById(ExpectedTo.Id)).Returns(ExpectedTo);
        }
    }
}
