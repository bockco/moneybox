﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using System;

namespace Moneybox.App.Features
{
    public class TransferMoney
    {
        private IAccountRepository accountRepository;
        private INotificationService notificationService;

        public TransferMoney(IAccountRepository accountRepository, INotificationService notificationService)
        {
            this.accountRepository = accountRepository;
            this.notificationService = notificationService;
        }

        public void Execute(Guid fromAccountId, Guid toAccountId, decimal amount)
        {
            var fromAccount = accountRepository.GetAccountById(fromAccountId);
            var toAccount = accountRepository.GetAccountById(toAccountId);

            ValidateAccountState(amount, fromAccount, toAccount);

            fromAccount.UpdateAccountForWithdraw(amount);
            toAccount.UpdateAccountForPaidin(amount);

            UpdateAccountRepository(fromAccount, toAccount);
        }

        private void UpdateAccountRepository(Account fromAccount, Account toAccount)
        {
            accountRepository.Update(fromAccount);
            accountRepository.Update(toAccount);
        }

        private void ValidateAccountState(decimal amount, Account fromAccount, Account toAccount)
        {
            if (fromAccount.InsufficientFunds(amount))
            {
                throw new InvalidOperationException("Insufficient funds to make transfer");
            }

            if (fromAccount.NotifyOfLowFunds(amount))
            {
                notificationService.NotifyFundsLow(fromAccount.User.Email);
            }

            if (toAccount.ReachedPayinLimit(amount))
            {
                throw new InvalidOperationException("Account pay in limit reached");
            }

            if (toAccount.NotifyOfPayinLimit(amount))
            {
                notificationService.NotifyApproachingPayInLimit(toAccount.User.Email);
            }
        }
    }
}
