﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using System;

namespace Moneybox.App.Features
{
    public class WithdrawMoney
    {
        private IAccountRepository accountRepository;
        private INotificationService notificationService;

        public WithdrawMoney(IAccountRepository accountRepository, INotificationService notificationService)
        {
            this.accountRepository = accountRepository;
            this.notificationService = notificationService;
        }

        public void Execute(Guid fromAccountId, decimal amount)
        {
            var fromAccount = accountRepository.GetAccountById(fromAccountId);

            ValidWithdrawState(amount, fromAccount);

            fromAccount.UpdateAccountForWithdraw(amount);

            accountRepository.Update(fromAccount);
        }

        private void ValidWithdrawState(decimal amount, Account fromAccount)
        {
            if (fromAccount.InsufficientFunds(amount))
            {
                throw new InvalidOperationException("Insufficient funds to make transfer");
            }

            if (fromAccount.NotifyOfLowFunds(amount))
            {
                notificationService.NotifyFundsLow(fromAccount.User.Email);
            }
        }
    }
}
