﻿using System;

namespace Moneybox.App
{
    public class Account
    {
        public const decimal PayInLimit = 4000m;

        public Guid Id { get; set; }

        public User User { get; set; }

        public decimal Balance { get; set; }

        public decimal Withdrawn { get; set; }

        public decimal PaidIn { get; set; }

        public bool ReachedPayinLimit(decimal amount)
        {
            if ((PaidIn + amount) > PayInLimit)
            {
                return true;
            }

            return false;
        }

        public bool NotifyOfPayinLimit(decimal amount)
        {
            if ((PayInLimit - (PaidIn - amount)) < 500m)
            {
                return true;
            }

            return false;
        }

        public bool InsufficientFunds(decimal amount)
        {
            if ((Balance - amount) < 0m)
            {
                return true;
            }

            return false;
        }

        public bool NotifyOfLowFunds(decimal amount)
        {
            if ((Balance - amount) < 500m)
            {
                return true;
            }

            return false;
        }

        public void UpdateAccountForPaidin(decimal amount)
        {
            Balance = Balance + amount;
            PaidIn = PaidIn + amount;
        }
        public void UpdateAccountForWithdraw(decimal amount)
        {
            Balance = Balance - amount;
            Withdrawn = Withdrawn - amount;
        }
    }
}
